package com.sinsz.pay.properties;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sinsz.pay.properties.support.WeChatPlatform;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * 微信支付配置
 * @author chenjianbo
 */
@Validated
@ConfigurationProperties("sinsz.pay.wxpay")
public class Wxpay {

    /**
     * 微信平台，OPEN表示开放平台，PUBLIC表示公众平台
     */
    @NotNull
    private WeChatPlatform platform = WeChatPlatform.OPEN;

    /**
     * 微信公众号ID或开放平台ID
     */
    @NotEmpty
    private String appid;

    /**
     * 微信公众号或开放平台对应密钥
     */
    @NotEmpty
    private String secret;

    /**
     * 商户号
     */
    @NotEmpty
    private String mchId;

    /**
     * 商户API密钥
     */
    @NotEmpty
    private String mchKey;

    /**
     * 商户证书文件地址，绝对路径
     */
    private String mchCer = "";

    /**
     * 支付结果回调
     */
    @NotEmpty
    private String payNotify;

    /**
     * 退款结果回调
     */
    private String refundNotify = "";

    /**
     * 门店ID
     */
    private String storeId = "";

    /**
     * 门店名称
     */
    private String storeName = "";

    /**
     * 微信公众号授权重定向地址设置
     * <p>
     *     举例：http://xxx.com/api/1.0/xxx则填写/api/1.0/xxx;
     *     如果不填写值则会返回openid值，如果填写这回重定向跳转到这个地址
     * </p>
     */
    private String weChatPublicNumberRedirect = "";

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getMchId() {
        return mchId;
    }

    public void setMchId(String mchId) {
        this.mchId = mchId;
    }

    public String getMchKey() {
        return mchKey;
    }

    public void setMchKey(String mchKey) {
        this.mchKey = mchKey;
    }

    public String getPayNotify() {
        return payNotify;
    }

    public void setPayNotify(String payNotify) {
        this.payNotify = payNotify;
    }

    public String getRefundNotify() {
        return refundNotify;
    }

    public void setRefundNotify(String refundNotify) {
        this.refundNotify = refundNotify;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getMchCer() {
        return mchCer;
    }

    public void setMchCer(String mchCer) {
        this.mchCer = mchCer;
    }

    public WeChatPlatform getPlatform() {
        return platform;
    }

    public void setPlatform(WeChatPlatform platform) {
        this.platform = platform;
    }

    public String getWeChatPublicNumberRedirect() {
        return weChatPublicNumberRedirect;
    }

    public void setWeChatPublicNumberRedirect(String weChatPublicNumberRedirect) {
        this.weChatPublicNumberRedirect = weChatPublicNumberRedirect;
    }

    @Override
    public String toString() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace(System.out);
        }
        return super.toString();
    }

}